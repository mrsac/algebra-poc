let {Expression, Equation} = require('algebra.js');
let algebra = require('algebra.js');

class ExpressionEvaluator{
  constructor() {
    this.specialCharacters = ["(", ")"];
    this.arithematicOperators = ["+", "/", "-", "*", "="];
    this.variableMap = {};
    this.variableEquation = {};
  }

  getMetadata(expressionStr = "(((FRV1C_1 - FRV1P) / FRV1P) * 100)") {
    try {
      expressionStr = expressionStr.replace(/ /g, "");
      let expression = expressionStr.split("");
      let self = this;
      let codes = [],
        characters = [],
        metadata = [],
        index = -1;
      for (let symbol of expressionStr) {
        ++index;
        if (
          (self.specialCharacters.includes(symbol) ||
            self.arithematicOperators.includes(symbol)) &&
          index === 0
        ) {
          index = -1;
          let char = expression.shift();
          //   characters.push(char);
          metadata.push(char);
        } else if (
          self.specialCharacters.includes(symbol) ||
          self.arithematicOperators.includes(symbol)
        ) {
          let code = expression.splice(0, index).join("");
          metadata.push(code);
          let char = expression.shift();
          //   characters.push(char);
          metadata.push(char);
          index = -1;
        }
      }
      if(expression.length){
        let code = expression.join("");
        metadata.push(code);
      }
      return (metadata);
    } catch (err) {
      throw err;
    }
  }

  nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
  }

  mapVariables(metadata){
    let key = 'a';
    for (let index in metadata){
      if(this.specialCharacters.includes(metadata[index]) || !isNaN(metadata[index])
        || this.arithematicOperators.includes(metadata[index])){
        continue;
      }
      this.variableMap[key] = metadata[index];
      metadata[index] = key;
      key = this.nextChar(key);
    }
    return metadata;
  }

  reformatEquation (expression) {
    let self = this;
    let metadata = this.getMetadata(expression);
    console.log('New Metadata', metadata);
    for( let index in metadata){
      if(this.specialCharacters.includes(metadata[index]) || !isNaN(metadata[index])
        || this.arithematicOperators.includes(metadata[index])){
        continue;
      }
      console.log('Index', self.variableMap[metadata[index]], metadata[index]);
      if(self.variableMap[metadata[index]]){
        metadata[index] = self.variableMap[metadata[index]]
      }
    }
    expression = metadata.join("");
    console.log('Reformat eqn', expression);
    return expression
  }

  expressionBuilder(expression, target = 50){
    try{
      let expr  = algebra.parse(expression);
      let eq = new Equation(expr, target);
      console.log('Equation', eq.solveFor('a'));
      for (let variable in this.variableMap){
        this.variableEquation[this.variableMap[variable]] = this.reformatEquation(eq.solveFor(variable).toString());
      }
      console.log('This is it, variable equation', this.variableEquation);
      return this.variableEquation;
    }catch (e) {
      console.log('Error in expression builder', e);
      throw e;
    }
  }



  evaluateExpression(expression){
    try{
      let self = this;
      console.log('Expression', expression);
      let metadata = self.getMetadata(expression);
      console.log('Metadata', metadata);
      let expr = self.mapVariables(metadata);
      console.log('Mapped Variables', self.variableMap);
      console.log('Remapped expr', expr);
      let exrpression = self.expressionBuilder(expr.join(""));
      return exrpression;
    } catch (err) {
      console.log('Error caught', err);
    }
  }
}

let expEval = new ExpressionEvaluator();
let res = expEval.evaluateExpression("A + B + 25", {});
